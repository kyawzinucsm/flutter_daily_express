import 'dart:convert';
import 'dart:core';
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Daily Express',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
          // This is the theme of your application.
          //
          // Try running your application with "flutter run". You'll see the
          // application has a blue toolbar. Then, without quitting the app, try
          // changing the primarySwatch below to Colors.green and then invoke
          // "hot reload" (press "r" in the console where you ran "flutter run",
          // or simply save your changes to "hot reload" in a Flutter IDE).
          // Notice that the counter didn't reset back to zero; the application
          // is not restarted.
          primarySwatch: Colors.red,
          backgroundColor: Colors.red),
      home: LoginScreen(),
    );
  }
}

class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final emailController = TextEditingController();
  final pwdController = TextEditingController();
  SharedPreferences prefs;
  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    // Clean up the controller when the widget is disposed.

    super.dispose();
    emailController.dispose();
    pwdController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    final GlobalKey<State> _keyLoader = new GlobalKey<State>();
    final logo = Center(
        child: CircleAvatar(
      backgroundImage: ExactAssetImage('assets/images/logo.png'),
      minRadius: 50,
      maxRadius: 50,
    ));

    final email = new TextField(
      textAlign: TextAlign.start,
      maxLines: 1,
      controller: emailController,
      decoration: new InputDecoration(
          contentPadding:
              EdgeInsets.only(top: 15.0, bottom: 15.0, left: 20.0, right: 40.0),
          hintText: 'Enter Your Email or Phone',
          border: new OutlineInputBorder(
            borderRadius: const BorderRadius.all(
              const Radius.circular(10.0),
            ),
            borderSide: new BorderSide(
              color: const Color(0xff7C7B7B),
              width: 1.0,
            ),
          ),
          focusedBorder: new OutlineInputBorder(
            borderRadius: const BorderRadius.all(
              const Radius.circular(10.0),
            ),
            borderSide: new BorderSide(
              color: const Color(0xff7C7B7B),
              width: 1.0,
            ),
          )),
    );

    final password = new TextField(
      textAlign: TextAlign.start,
      maxLines: 1,
      obscureText: true,
      controller: pwdController,
      decoration: new InputDecoration(
          contentPadding:
              EdgeInsets.only(top: 15.0, bottom: 15.0, left: 20.0, right: 40.0),
          fillColor: Colors.white,
          filled: true,
          hintText: 'Enter Your Password',
          border: new OutlineInputBorder(
            borderRadius: const BorderRadius.all(
              const Radius.circular(10.0),
            ),
            borderSide: new BorderSide(
              color: const Color(0xff7C7B7B),
              width: 1.0,
            ),
          ),
          focusedBorder: new OutlineInputBorder(
            borderRadius: const BorderRadius.all(
              const Radius.circular(10.0),
            ),
            borderSide: new BorderSide(
              color: const Color(0xff7C7B7B),
              width: 1.0,
            ),
          )),
    );
    final loginButton = Padding(
      padding: EdgeInsets.symmetric(vertical: 16.0),
      child: RaisedButton(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10),
        ),
        onPressed: () async {
          Dialogs.showLoadingDialog(context,
              _keyLoader); //invoking login https://secret-fjord-40447.herokuapp.com
          var url = 'http://192.168.1.12:8080/login';
          // var url = 'http://192.168.1.10:8080/login';
          Map loginData = {
            "email": emailController.text,
            "password": pwdController.text
          };
          var body = json.encode(loginData);
          var response = await http.post(url,
              headers: {"Content-Type": "application/json"}, body: body);
          Navigator.of(_keyLoader.currentContext, rootNavigator: true).pop();
          if (response.statusCode == 200) {
            prefs = await SharedPreferences.getInstance();
            Map<String, dynamic> aa = jsonDecode(response.body);
            await prefs.setString("accId", aa['userId']);
          } else {
            print(" Login failed" + response.body);
          }
        },
        padding: EdgeInsets.only(top: 15, bottom: 15),
        color: Color(0xffb2002d),
        child: Text('Log In',
            style: TextStyle(
                color: Colors.white,
                fontSize: 15,
                fontWeight: FontWeight.bold)),
      ),
    );

    final forgotLabel = Padding(
      padding: EdgeInsets.only(left: 120.0),
      child: RaisedButton(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(50),
        ),
        onPressed: () {},
        padding: EdgeInsets.all(8),
        color: Color(0xffFFFFFF),
        child: Text('Forgot password?',
            style: TextStyle(
                color: Color(0xff4E4040), fontWeight: FontWeight.bold)),
      ),
    );
    final createAccount = new Row(
      mainAxisAlignment: MainAxisAlignment.center,
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        GestureDetector(
          child: new Text('Don\'t have an account?',
              style: new TextStyle(color: Colors.black, fontSize: 16)),
          onTap: () {},
        ),
        new FlatButton(
          onPressed: () {
            print("Hello sign up");
          },
          child: new Text("SIGN UP",
              style: TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                  fontSize: 16)),
        )
      ],
    );

    return SafeArea(child: Scaffold(
      backgroundColor: Colors.red[300],
      resizeToAvoidBottomPadding: true,
      body: Stack(
        children: <Widget>[
          // Center(
          //   child: new Image.asset(
          //     'assets/images/homebg.png',
          //     width: size.width,
          //     height: size.height,
          //     fit: BoxFit.fill,
          //   ),
          // ),
          Center(
              child: SingleChildScrollView(
            child: ListView(
              shrinkWrap: true,
              padding: EdgeInsets.only(left: 40.0, right: 40.0, top: 20.0),
              children: <Widget>[
                SizedBox(height: 40.0),
                new Container(
                    width: 100.0,
                    height: 100.0,
                    decoration: new BoxDecoration(
                        shape: BoxShape.rectangle,
                        image: new DecorationImage(
                            fit: BoxFit.contain,
                            image: new AssetImage("assets/images/logo.png")))),
                SizedBox(height: 40.0),
                Material(
                    elevation: 3,
                    color: Colors.white,
                    shadowColor: Color(0xff4E4040),
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.all(Radius.circular(10.0))),
                    child: email),
                SizedBox(height: 27.0),
                Material(
                    elevation: 3,
                    color: Colors.white,
                    shadowColor: Color(0xff4E4040),
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.all(Radius.circular(10.0))),
                    child: password),
                SizedBox(height: 10.0),
                loginButton,
                SizedBox(height: 8.0),
                forgotLabel,
                SizedBox(height: 40.0),
                createAccount
              ],
            ),
          ))
        ],
      ),
    )
  );}
}

class Dialogs {
  static Future<void> showLoadingDialog(
      BuildContext context, GlobalKey key) async {
    return showDialog<void>(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return new WillPopScope(
              onWillPop: () async => false,
              child: SimpleDialog(
                  key: key,
                  backgroundColor: Colors.black54,
                  children: <Widget>[
                    Center(
                      child: Column(children: [
                        CircularProgressIndicator(),
                        SizedBox(
                          height: 10,
                        ),
                        Text(
                          "Please Wait....",
                          style: TextStyle(color: Colors.blueAccent),
                        )
                      ]),
                    )
                  ]));
        });
  }
}
